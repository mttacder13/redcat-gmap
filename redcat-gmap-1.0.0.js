/**
 * redcat's jQuery-GoogleMap library
 * Requires
 *     jQuery
 *     $.msgbox - http://mttacder13.bitbucket.org/redcat-msgbox
 *     Google Maps Javascript API v3 - http://maps.googleapis.com/maps/api/js?key=AIzaSyDs3US5PdqAMoIaoBBYHF-fACFLXFtZaMg&sensor=false
 *
 * For more info about Google Maps, visit: https://developers.google.com/maps/documentation/javascript/tutorial
 */
(function() {

    /**
     * This is generally for namespacing, see $.gmap.i18n and $.gmap.defaults
     * but you can also use this to initialize gmap.
     * @param  {string} selector the selector for jQuery
     * @param  {object} options  the options for gmap, see $.gmap.defaults
     * @return {GoogleMap}
     */
    $.gmap = function(selector, options) {
        return $(selector).gmap(options);
    }

    /**
     * This is the jQuery plugin so we can use the standard $(selector).gmap(options);
     * @param  {object} options
     * @return {GoogleMap}
     */
    $.fn.gmap = function(options) {
        if (this.length > 1) {
            var gmaps = [];
            this.each(function() {
                gmaps.push($(this).gmap(options));
            });
            return gmaps;
        }
        return new GoogleMap(this, options || {});
    }

    /**
     * @class GoogleMap
     * @param {jQuery} $element the jQuery wrapped element
     * @param {object} options  contains the options for GoogleMap as well as the map itself
     */
    function GoogleMap($element, options) {

        this.element = $element[0];
        this.options = options;
        this.options.mapOptions = $.extend({}, $.gmap.defaults.mapOptions, options.mapOptions);
        this.options = $.extend({}, $.gmap.defaults, this.options);
        this.map = new google.maps.Map(this.element, this.options.mapOptions);
        this.geocoder = new google.maps.Geocoder();
        this.infoWindow = new google.maps.InfoWindow();
        this.markerControls = {};
        this.mapMarkers = [];
        this.clickHandlers = [];

        this.addFindLocationControl();
        this.addMarkerControls();
        this.addEventListeners();

        $element.addClass('gmap').css({
            display: 'block',
            width: $element.width() || this.options.width,
            height: $element.height() || this.options.height
        });
    }

    GoogleMap.prototype.addFindLocationControl = function() {
        if (!this.options.allowFind) {
            return;
        }

        var self = this;
        var container = document.createElement('div');
        container.style.padding = '0 5px';

        var findLocationControl = document.createElement('div');
        findLocationControl.setAttribute('style', this.options.mapCustomControlStyles);
        findLocationControl.innerHTML = $.gmap.i18n.findLocationControlText;
        findLocationControl.title = $.gmap.i18n.findLocationControlTitle;
        container.appendChild(findLocationControl);

        google.maps.event.addDomListener(findLocationControl, 'click', function() {
            $.msgbox.prompt($.gmap.i18n.findLocationControlPromptText, $.gmap.i18n.findLocationControlPromptTitle, function(value) {
                if (value) {
                    var msgbox = $.msgbox($.gmap.i18n.findLocationControlFindingText);
                    self.geocodeAddress(value, function(latLng, results, status) {
                        msgbox.close();
                        if (latLng) {
                            self.center(latLng);
                            self.map.setZoom(13);
                        } else {
                            $.msgbox([$.gmap.i18n.findLocationControlFailed, value]);
                        }
                    });
                }
            });
        });

        this.map.controls[google.maps.ControlPosition.RIGHT_TOP].push(container);
    }

    GoogleMap.prototype.addMarkerControls = function() {

        var self = this;
        if (!self.options.enableMarkerControls) {
            return;
        }

        var markerControls = document.createElement('div');
        markerControls.style.padding = '0 5px';

        var markersText = document.createElement('div');
        markersText.setAttribute('style', this.options.mapCustomControlStyles + 'display:inline-block;');
        markersText.innerHTML = $.gmap.i18n.markersLabel;
        markersText.style.display = 'inline-block';
        markerControls.appendChild(markersText);

        google.maps.event.addDomListener(markersText, 'click', function() {
            addMarkerControl.enabled = true;
            deleteMarkerControl.enabled = true;
            addMarkerControl.toggleState();
            deleteMarkerControl.toggleState();
        });

        var addMarkerControl = document.createElement('div');
        addMarkerControl.setAttribute('style', this.options.mapCustomControlStyles + 'display:inline-block;min-width:5px;font-weight:bold;');
        addMarkerControl.innerHTML = '+';
        addMarkerControl.title = $.gmap.i18n.addMarkersControlTitle;
        addMarkerControl.enabled = true;
        addMarkerControl.toggleState = function() {
            if (addMarkerControl.enabled) {
                addMarkerControl.enabled = false;
                addMarkerControl.style.backgroundColor = '#fff';
                self.map.setOptions({ draggableCursor: null });
            } else {
                deleteMarkerControl.enabled = true;
                deleteMarkerControl.toggleState();

                addMarkerControl.enabled = true;
                addMarkerControl.style.backgroundColor = '#ddd';
                self.map.setOptions({draggableCursor: 'crosshair'});
            }
        }
        addMarkerControl.toggleState();
        markerControls.appendChild(addMarkerControl);
        google.maps.event.addDomListener(addMarkerControl, 'click', addMarkerControl.toggleState);

        var deleteMarkerControl = document.createElement('div');
        deleteMarkerControl.setAttribute('style', this.options.mapCustomControlStyles + 'display:inline-block;min-width:5px;font-weight:bold;');
        deleteMarkerControl.innerHTML = '-';
        deleteMarkerControl.title = $.gmap.i18n.deleteMarkersControlTitle;
        deleteMarkerControl.enabled = true;
        deleteMarkerControl.toggleState = function() {
            if (deleteMarkerControl.enabled) {
                deleteMarkerControl.enabled = false;
                deleteMarkerControl.style.backgroundColor = '#fff';
                self.map.setOptions({ draggableCursor: null });
            } else {
                addMarkerControl.enabled = true;
                addMarkerControl.toggleState();

                deleteMarkerControl.enabled = true;
                deleteMarkerControl.style.backgroundColor = '#ddd';
                self.map.setOptions({ draggableCursor: 'default' });
            }
        }
        deleteMarkerControl.toggleState();
        google.maps.event.addDomListener(deleteMarkerControl, 'click', deleteMarkerControl.toggleState);
        markerControls.appendChild(deleteMarkerControl);

        var clearAllMarkersControl = document.createElement('div');
        clearAllMarkersControl.setAttribute('style', this.options.mapCustomControlStyles + 'display:inline-block;min-width:5px;font-weight:bold;');
        clearAllMarkersControl.innerHTML = '&times;';
        clearAllMarkersControl.title = $.gmap.i18n.clearAllMarkersControlTitle;
        markerControls.appendChild(clearAllMarkersControl);
        google.maps.event.addDomListener(clearAllMarkersControl, 'click', function() {
            if (self.mapMarkers.length) {
                $.msgbox.ask($.gmap.i18n.clearAllMarkersControlPromptText, $.gmap.i18n.clearAllMarkersControlPromptTitle, function() {
                    self.clearAllMarkers();
                });
            } else {
                $.msgbox($.gmap.i18n.clearAllMarkersControlPromptFailed);
            }
        });

        this.map.controls[google.maps.ControlPosition.RIGHT_TOP].push(markerControls);
        this.markerControls.addMarkerControl = addMarkerControl;
        this.markerControls.deleteMarkerControl = deleteMarkerControl;
        this.markerControls.clearMarkersControl = clearAllMarkersControl;
    }

    GoogleMap.prototype.addEventListeners = function() {
        var self = this;
        google.maps.event.addListener(self.map, 'click', function(e) {
            self.geocoder.geocode({ latLng: e.latLng }, function(results, status) {
                $.each(self.clickHandlers, function(i, handler) {
                    // for more info: https://developers.google.com/maps/documentation/javascript/geocoding
                    handler(
                        // pass the first result if there is any
                        (status == google.maps.GeocoderStatus.OK ? results[0] : false),
                        // pass the original clicked coordinates
                        e.latLng,
                        // pass all the other results
                        results,
                        // pass the actual status code
                        status
                        );
                });
            });
        });

        if (self.options.enableMarkerControls) {
            google.maps.event.addListener(self.map, 'click', function(e) {
                if (self.markerControls.addMarkerControl.enabled) {
                    self.geocoder.geocode({ latLng: e.latLng }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            $.msgbox.ask('<strong>' + results[0].formatted_address + '</strong>', $.gmap.i18n.addMarkersControlPromptTitle, function() {
                                self.addMarkerByLatLng(e.latLng, results[0].formatted_address);
                            });
                        } else {
                            $.msgbox([$.gmap.i18n.geocoderFailedMessage, status]);
                        }
                    });
                }
            });
        }
    }

    GoogleMap.prototype.geocodeAddress = function(address, callback) {
        this.geocoder.geocode({ address: address }, function(results, status) {
            callback(
                // pass the first LatLng object found or false if not found
                (status == google.maps.GeocoderStatus.OK ? results[0].geometry.location : false),
                // pass all the other results (array)
                results,
                // pass the actual status code
                status
                );
        })
    }

    GoogleMap.prototype.addMarkerByAddress = function(address, title, successCallback) {

        if (typeof title == 'function') {
            successCallback = title;
            title = address;
        }

        this.geocodeAddress(address, function(latLng, results, status) {
            if (latLng) {
                var markerIndex = this.addMarkerByLatLng(latLng, title);
                successCallback(markerIndex);
            }
        });
    }

    GoogleMap.prototype.addMarkerByLatLng = function(lat, lng, title, center) {

        if (this.options.maxMarkersCount != -1 && this.mapMarkers.length >= this.options.maxMarkersCount) {
            $.msgbox([$.gmap.i18n.maxMarkersReached, this.options.maxMarkersCount]);
            return false;
        }

        var latLng = lat;
        if (typeof title == 'boolean') {
            center = title;
        }
        if (typeof lng == 'string') {
            title = lng;
        }
        if (typeof lat == 'number') {
            latLng = new google.maps.LatLng(lat, lng);
        }
        if (title == undefined) {
            title = $.gmap.i18n.defaultTitle;
        }

        var self = this;
        var marker = new google.maps.Marker({
            position: latLng,
            map: self.map,
            listeners: [],
            content: title,
            gmap: this
        });
        self.mapMarkers.push(marker);
        marker.listeners.push(google.maps.event.addListener(marker, 'click', function() {
            if (self.markerControls.deleteMarkerControl.enabled) {
                var marker = this;
                $.msgbox.ask('<strong>' + marker.content + '</strong>', $.gmap.i18n.deleteMarkersControlPromptTitle, function() {
                    self.removeMarker(marker);
                });
            }
        }));
        marker.listeners.push(google.maps.event.addListener(marker, 'mouseover', function() {
            this.gmap.infoWindow.setContent(this.content);
            this.gmap.infoWindow.open(this.map, this);
        }));
        marker.listeners.push(google.maps.event.addListener(marker, 'mouseout', function() {
            this.gmap.infoWindow.close();
        }));

        if (center) {
            self.center(latLng);
        }

        return marker;
    }

    GoogleMap.prototype.removeMarker = function(index) {
        var marker = index;
        if (typeof marker != 'number') {
            index = this.mapMarkers.indexOf(marker);
        }
        if (this.mapMarkers[index]) {
            marker = this.mapMarkers[index];
            this.mapMarkers[index].setMap(null);
            this.mapMarkers.splice(index, 1);
            $.each(marker.listeners, function(i, l) {
                google.maps.event.removeListener(l);
            });
        }
    }

    GoogleMap.prototype.clearAllMarkers = function() {
        for (var i = this.mapMarkers.length - 1; i >= 0; i--) {
            this.removeMarker(i);
        }
    }

    GoogleMap.prototype.center = function(lat, lng) {
        var latLng = lat;

        if (typeof lng == 'number') {
            latLng = new google.maps.LatLng(lat, lng);
        }

        this.map.setCenter(latLng);
    }

    GoogleMap.prototype.addClickHandlers = function() {
        for (var i = 0; i < arguments.length; i++) {
            this.clickHandlers.push(arguments[i]);
        };
    }

})();

$.gmap.i18n = {
    defaultTitle: 'This Location',
    maxMarkersReached: 'Maximum number of markers reached! You cannot add more than {0} marker(s).',
    confirmAddMarkerMessage: 'Are you sure you want to add marker at this location?',
    findLocationControlText: 'Find Location',
    findLocationControlTitle: 'Input address to locate in this map',
    findLocationControlPromptText: 'Enter the address you want to locate:',
    findLocationControlPromptTitle: 'Input Address',
    findLocationControlFindingText: 'Locating address. Please wait...',
    findLocationControlFailed: 'Unable to find address: "{0}"',
    markersLabel: 'Markers: ',
    addMarkersControlTitle: 'Add Marker to a location',
    addMarkersControlPromptTitle: 'Mark this Location?',
    deleteMarkersControlTitle: 'Select and delete marker',
    deleteMarkersControlPromptTitle: 'Delete Marker?',
    clearAllMarkersControlTitle: 'Clear all markers',
    clearAllMarkersControlPromptText: 'Are you sure you want to clear all markers?',
    clearAllMarkersControlPromptTitle: 'Clear All Markers',
    clearAllMarkersControlPromptFailed: 'There are no location markers on this map.',
    geocoderFailedMessage: 'Geocoder failed: "{0}"'
}

$.gmap.defaults = {
    mapOptions: {
        zoom: 8,
        zoomControl: true,
        scaleControl: true,
        navigationControl: true,
        mapTypeControl: true,
        scrollwheel: false,
        draggable: true,
        center: new google.maps.LatLng(10.327817594973057, 123.90623599290848),
        mapTypeId: google.maps.MapTypeId.ROADMAP
        // for more options and explanation of these options, see https://developers.google.com/maps/documentation/javascript/reference?hl=en#MapOptions
    },
    width: 500,
    height: 300,
    /**
     * Whether to show the marker controls (add, remove and clear)
     * @type {Boolean}
     */
    enableMarkerControls: false,
    /**
     * The maximum number of markers allowed.
     *     0  no markers allowed
     *     -1 unlimited
     * @type {Number}
     */
    maxMarkersCount: 0,
    /**
     * These default styles are copied from the actual existing map button control
     * @type {String}
     */
    mapCustomControlStyles: 'cursor:pointer;font-size:11px;background-color:#fff;padding:1px 6px;border-bottom-right-radius:2px;border-top-right-radius:2px;background-clip:padding-box;border-width:1px 1px 1px 1px;border-style:solid solid solid solid;border-color:rgba(0,0,0,0.15) rgba(0,0,0,0.15) rgba(0,0,0,0.15) -moz-use-text-color;-moz-border-top-colors:none;-moz-border-right-colors:none;-moz-border-bottom-colors:none;-moz-border-left-colors:none;border-image:none;box-shadow:0px 1px 4px -1px rgba(0,0,0,0.3);min-width:38px;',
    /**
     * Allow the user to find the location by inputting a string address
     * @type {Boolean}
     */
    allowFind: true
}
